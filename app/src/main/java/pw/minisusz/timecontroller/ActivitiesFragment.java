package pw.minisusz.timecontroller;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.List;

import pw.minisusz.timecontroller.Adapters.ItemAdapter;
import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Services.IActivityService;


/**
 * A simple {@link Fragment} subclass.
 */
public class ActivitiesFragment extends Fragment {

    List<ActivityItem> items;
    IActivityService service;
    GridView gridView;
    ItemAdapter adapter;

    public ActivitiesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_activities, container, false);
        service = ((ActivitiesActivity) getActivity()).getService();
        items = service.getAllItems();
        service.setCurrentItem(null);
        adapter = new ItemAdapter(getActivity(), items);
        gridView = (GridView) view.findViewById(R.id.activities);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                service.setCurrentItem(items.get(position));
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.activity_fragment, new EditActivityFragment());
                ft.addToBackStack("ActivitiesFragment");
                ft.commit();
            }
        });
        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deletingItemDialog(position);
                return true;
            }
        });
        FloatingActionButton fbAdd = (FloatingActionButton) view.findViewById(R.id.fb_add_activity);
        fbAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.activity_fragment, new EditActivityFragment());
                ft.addToBackStack("ActivitiesFragment");
                ft.commit();
            }
        });
        return view;
    }

    private void deletingItemDialog(int position) {
        service.setCurrentItem(items.get(position));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you want to delete " + items.get(position).name + " activity?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        service.deleteItem();
                        items.remove(position);
                        adapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
