package pw.minisusz.timecontroller;

import android.app.Dialog;

import java.util.Calendar;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import pw.minisusz.timecontroller.Adapters.HistoryPagerAdapter;
import pw.minisusz.timecontroller.Services.HistoryService;
import pw.minisusz.timecontroller.Services.IHistoryService;

public class HistoryActivity extends BaseActivity {

    private IHistoryService historyService;
    FragmentPagerAdapter adapterViewPager;
    ViewPager viewPager;

    public IHistoryService getHistoryService() {
        return historyService;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_history, frameLayout);
        setTitle("History");
        historyService = new HistoryService(getApplication());
        Calendar calendar = Calendar.getInstance();
        historyService.setChoosenDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        viewPager = (ViewPager) findViewById(R.id.history_view_pager);
        adapterViewPager = new HistoryPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TabLayout tabLayout = (TabLayout) findViewById(R.id.history_tabs);
        tabLayout.setupWithViewPager(viewPager);
        FloatingActionButton fbDateHistory = (FloatingActionButton) findViewById(R.id.fb_date_history);
        fbDateHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(1).setChecked(true);
    }

    private void showDatePicker() {
        final Dialog dialog = new Dialog(this, R.style.DialogTheme);
        dialog.setContentView(R.layout.dialog_date_picker);

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        datePicker.setMaxDate(System.currentTimeMillis());
        Button btnSave = (Button) dialog.findViewById(R.id.btn_save_date);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyService.setChoosenDate(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                adapterViewPager.notifyDataSetChanged();
                dialog.cancel();
            }
        });
        dialog.show();
    }
}
