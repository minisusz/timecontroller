package pw.minisusz.timecontroller;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import pw.minisusz.timecontroller.Services.ActivityService;
import pw.minisusz.timecontroller.Services.IActivityService;

public class ActivitiesActivity extends BaseActivity {

    private IActivityService service;

    public IActivityService getService() {
        return service;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_activities, frameLayout);
        setTitle("Activities");
        service = new ActivityService(getApplication());
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.activity_fragment, new ActivitiesFragment());
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(2).setChecked(true);
    }
}
