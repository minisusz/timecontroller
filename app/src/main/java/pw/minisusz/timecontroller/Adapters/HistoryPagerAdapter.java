package pw.minisusz.timecontroller.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pw.minisusz.timecontroller.HistoryActivityDayFragment;
import pw.minisusz.timecontroller.HistoryLocationDayFragment;

/**
 * Created by Lukasz on 2017-05-15.
 */


public class HistoryPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 2;
    private String tabTitles[] = new String[]{"Activities", "Locations"};

    public HistoryPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0
                return HistoryActivityDayFragment.newInstance(0, "Activities");
            case 1: // Fragment # 1
                return HistoryLocationDayFragment.newInstance(1, "Locations");
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
