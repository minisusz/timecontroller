package pw.minisusz.timecontroller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import pw.minisusz.timecontroller.Model.AreaItem;
import pw.minisusz.timecontroller.R;

/**
 * Created by Lukasz on 2017-05-19.
 */

public class AreaAdapter extends ArrayAdapter<AreaItem> {

    private Context ctx;
    private List<AreaItem> list;

    public AreaAdapter(Context context, List<AreaItem> list) {
        super(context, R.layout.area_layout);
        this.ctx = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.area_layout, parent, false);

        TextView name = (TextView) row.findViewById(R.id.area_item_name);
        name.setText(list.get(position).name);

        TextView latitude = (TextView) row.findViewById(R.id.area_item_latitude);
        latitude.setText(String.valueOf(list.get(position).latitude));

        TextView longitude = (TextView) row.findViewById(R.id.area_item_longitude);
        longitude.setText(String.valueOf(list.get(position).longitude));


        return row;
    }
}
