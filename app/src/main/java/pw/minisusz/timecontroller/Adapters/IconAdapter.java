package pw.minisusz.timecontroller.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.Arrays;

import pw.minisusz.timecontroller.R;

/**
 * Created by Lukasz on 2017-05-06.
 */

public class IconAdapter extends ArrayAdapter<Integer> {

    Integer[] icons;
    int color;
    Context context;

    public IconAdapter(Context context, Integer[] icons) {
        super(context, R.layout.icon_layout);
        this.icons = icons;
        this.context = context;
    }

    public void setColor(int color1) {
        color = color1;
    }

    @Override
    public int getCount() {
        return icons.length;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = getCustomView(position, convertView, parent);
        view.setBackgroundColor(color);
        return view;
    }

    @Override
    public int getPosition(@Nullable Integer item) {
        if (item == null)
            return -1;
        return Arrays.asList(icons).indexOf(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.icon_layout, parent, false);

        ImageView imageView = (ImageView) row.findViewById(R.id.icon_image);
        imageView.setImageResource(icons[position]);

        return row;
    }
}
