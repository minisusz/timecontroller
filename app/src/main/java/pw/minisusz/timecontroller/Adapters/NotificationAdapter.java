package pw.minisusz.timecontroller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.R;
import pw.minisusz.timecontroller.TimeHelper;

/**
 * Created by Lukasz on 2017-05-16.
 */

public class NotificationAdapter extends ArrayAdapter<NotificationItem> {

    private Context ctx;
    private List<NotificationItem> list;

    public NotificationAdapter(Context context, List<NotificationItem> list) {
        super(context, R.layout.notification_layout);
        this.ctx = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.notification_layout, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.notification_time);
        textView.setText(TimeHelper.printTime(list.get(position).limitDay));

        TextView text = (TextView) row.findViewById(R.id.notification_textView);
        text.setText(list.get(position).text);

        ImageView imageView = (ImageView) row.findViewById(R.id.icon_activity);
        imageView.setImageResource(list.get(position).activityItem.icon);
        imageView.setBackgroundColor(list.get(position).activityItem.background);

        TextView textViewActivity = (TextView) row.findViewById(R.id.name_activity);
        textViewActivity.setText(list.get(position).activityItem.name);
        textViewActivity.setBackgroundColor(list.get(position).activityItem.background);


        return row;
    }
}
