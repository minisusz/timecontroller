package pw.minisusz.timecontroller.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pw.minisusz.timecontroller.HistoryAreaViewHolder;
import pw.minisusz.timecontroller.Model.HistoryAreaItem;
import pw.minisusz.timecontroller.R;

/**
 * Created by Lukasz on 2017-05-18.
 */

public class LegendAreaAdapter extends RecyclerView.Adapter<HistoryAreaViewHolder> {
    private List<HistoryAreaItem> list;

    public LegendAreaAdapter(List<HistoryAreaItem> list) {
        this.list = list;
    }

    @Override
    public HistoryAreaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.legend_area_layout, null);

        return new HistoryAreaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAreaViewHolder holder, int position) {
        HistoryAreaItem historyActivityItem = list.get(position);
        holder.legendName.setText(String.format("%d. %s", position + 1, historyActivityItem.getLocationItem().name));
        holder.legendValue.setText(historyActivityItem.getStringTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
