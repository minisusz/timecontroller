package pw.minisusz.timecontroller.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.R;

/**
 * Created by RAPTOR on 05.05.2017.
 */

public class ItemAdapter extends ArrayAdapter<ActivityItem> {
    private Context ctx;
    private List<ActivityItem> list;

    public ItemAdapter(Context context, List<ActivityItem> list) {
        super(context, R.layout.item_layout);
        this.ctx = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.item_layout, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.name_activity);
        textView.setText(list.get(position).name);

        ImageView imageView = (ImageView) row.findViewById(R.id.icon_activity);
        imageView.setImageResource(list.get(position).icon);

        row.setBackgroundColor(list.get(position).background);

        return row;
    }
}
