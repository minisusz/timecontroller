package pw.minisusz.timecontroller.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pw.minisusz.timecontroller.HistoryItemViewHolder;
import pw.minisusz.timecontroller.Model.HistoryActivityItem;
import pw.minisusz.timecontroller.R;

/**
 * Created by RAPTOR on 07.05.2017.
 */

public class LegendAdapter extends RecyclerView.Adapter<HistoryItemViewHolder> {
    private List<HistoryActivityItem> list;

    public LegendAdapter(List<HistoryActivityItem> list) {
        this.list = list;
    }

    @Override
    public HistoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.legend_item_layout, null);

        return new HistoryItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryItemViewHolder holder, int position) {
        HistoryActivityItem historyActivityItem = list.get(position);
        holder.legendName.setText(String.format("%d. %s", position + 1, historyActivityItem.getActivityItem().name));
        holder.legendValue.setText(historyActivityItem.getStringTime());
        holder.viewIcon.setBackgroundColor(historyActivityItem.getActivityItem().background);
        holder.legendIcon.setImageResource(historyActivityItem.getActivityItem().icon);
        holder.legendNameIcon.setText(historyActivityItem.getActivityItem().name);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
