package pw.minisusz.timecontroller;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Services.IHomeService;

/**
 * Created by RAPTOR on 23.04.2017.
 */

public class HomeActiveFragment extends Fragment {

    private TextView timeTextView;
    private Button stopButton;
    private Button cancelButton;
    private IHomeService timerService;
    private MyTimerTask myTimerTask;
    private Timer timer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_active, container, false);
        timerService = ((HomeActivity) getActivity()).getHomeService();
        timeTextView = (TextView) view.findViewById(R.id.time_textview);
        stopButton = (Button) view.findViewById(R.id.stop_button);
        cancelButton = (Button) view.findViewById(R.id.cancel_button);
        ActivityItem activityItem = timerService.getActiveItem();
        TextView textView = (TextView) view.findViewById(R.id.name_activity);
        textView.setText(activityItem.name);

        ImageView imageView = (ImageView) view.findViewById(R.id.icon_activity);
        imageView.setImageResource(activityItem.icon);

        View row = view.findViewById(R.id.item_layout);
        row.setBackgroundColor(activityItem.background);

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerService.setEndTime(Calendar.getInstance().getTimeInMillis());
                backToNoActiveFragment();
                updateWidget();
                AlarmReceiver alarmReceiver = new AlarmReceiver();
                alarmReceiver.cancelAlarm(getActivity());
                Toast.makeText(getActivity(), "Zapisano", Toast.LENGTH_LONG).show();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerService.cancel();
                backToNoActiveFragment();
                updateWidget();
                AlarmReceiver alarmReceiver = new AlarmReceiver();
                alarmReceiver.cancelAlarm(getActivity());
                Toast.makeText(getActivity(), "Anulowano", Toast.LENGTH_LONG).show();
            }
        });

        resumeTimer();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        timerService.updateHome();
        if (!timerService.isActive()) {
            backToNoActiveFragment();
        }

        resumeTimer();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void backToNoActiveFragment() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_home, new HomeNoActiveFragment());
        ft.commit();
    }

    private void updateWidget() {
        Intent intent = new Intent(getActivity(), TimerWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getActivity());
        ComponentName thisWidget = new ComponentName(getActivity(), TimerWidgetProvider.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        getActivity().sendBroadcast(intent);
    }

    private void resumeTimer() {
        long startTime = timerService.getStartTime();
        myTimerTask = new MyTimerTask(startTime);
        if (timer == null) {
            timer = new Timer();
            timer.schedule(myTimerTask, 0, 1000);
        }

    }

    private class MyTimerTask extends TimerTask {

        private long startTime;

        MyTimerTask(long startTime) {
            super();
            this.startTime = startTime;
        }

        @Override
        public void run() {
            long currentTime = Calendar.getInstance().getTimeInMillis();
            final String diffTime = printDifference(startTime, currentTime);

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timeTextView.setText(diffTime);
                }
            });
        }

        private String printDifference(long startDate, long endDate) {

            //milliseconds
            long different = endDate - startDate;

            return TimeHelper.printTime(different);
        }
    }
}
