package pw.minisusz.timecontroller;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.util.Calendar;

import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Services.AreaService;
import pw.minisusz.timecontroller.Services.HomeService;
import pw.minisusz.timecontroller.Services.IAreaService;
import pw.minisusz.timecontroller.Services.IHomeService;

/**
 * Created by RAPTOR on 08.05.2017.
 */

public class TimerWidgetProvider extends AppWidgetProvider {

    private static final String ACTION_STOP_TIMER_CLICK =
            "pw.minisusz.timecontroller.action.STOP_TIMER_CLICK";

    private PendingIntent getPendingSelfIntent(Context context, String action) {
        // An explicit intent directed at the current class (the "self").
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        final int N = appWidgetIds.length;
        IHomeService homeService = new HomeService(context);

        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];

            RemoteViews views;

            if (homeService.isActive()) {
                views = getViewActiveTimer(context, homeService);
            } else {
                views = getViewHomeTimer(context);
            }

            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    private RemoteViews getViewHomeTimer(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_timer_layout);
        views.setOnClickPendingIntent(R.id.widget_home_button, pendingIntent);
        IAreaService areaService = new AreaService(context);
        views.setTextViewText(R.id.widget_location_textview, areaService.getNameCurrentArea());

        return views;
    }

    private RemoteViews getViewActiveTimer(Context context, IHomeService homeService) {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_active_layout);
        ActivityItem activityItem = homeService.getActiveItem();
        views.setTextViewText(R.id.widegt_name_activity, activityItem.name);
        views.setImageViewResource(R.id.widget_icon_activity, activityItem.icon);
        views.setOnClickPendingIntent(R.id.widget_stop_button,
                getPendingSelfIntent(context,
                        ACTION_STOP_TIMER_CLICK));
        Intent intent = new Intent(context, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.widget_view_icon, pendingIntent);
        views.setInt(R.id.widget_view_icon, "setBackgroundColor", activityItem.background);
        IAreaService areaService = new AreaService(context);
        views.setTextViewText(R.id.widget_location_textview, areaService.getNameCurrentArea());

        return views;
    }

    private void onUpdate(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance
                (context);

        ComponentName thisAppWidgetComponentName =
                new ComponentName(context.getPackageName(), getClass().getName()
                );
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                thisAppWidgetComponentName);
        onUpdate(context, appWidgetManager, appWidgetIds);
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (ACTION_STOP_TIMER_CLICK.equals(intent.getAction())) {
            IHomeService homeService = new HomeService(context);
            homeService.setEndTime(Calendar.getInstance().getTimeInMillis());
            AlarmReceiver alarmReceiver = new AlarmReceiver();
            alarmReceiver.cancelAlarm(context);
            onUpdate(context);
        }
    }
}
