package pw.minisusz.timecontroller;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import pw.minisusz.timecontroller.Services.AreaService;
import pw.minisusz.timecontroller.Services.IAreaService;

public class LocationsActivity extends BaseActivity {

    private IAreaService areaService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        areaService = new AreaService(this);
        getLayoutInflater().inflate(R.layout.activity_locations, frameLayout);
        setTitle("Locations");
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(4).setChecked(true);
    }

    public IAreaService getAreaService() {
        return areaService;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.locations_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.locations_settings: {
                Intent intent = new Intent();
                intent.setClassName(this, "pw.minisusz.timecontroller.LocationsSettingsActivity");
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
