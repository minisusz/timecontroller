package pw.minisusz.timecontroller;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.List;

import pw.minisusz.timecontroller.Adapters.ItemAdapter;
import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.Services.IActivityService;
import pw.minisusz.timecontroller.Services.INotificationService;

/**
 * Created by Lukasz on 2017-05-16.
 */

public class EditNotificationFragment extends Fragment {

    private int hours;
    private int minutes;
    boolean isAddingMode;
    ItemAdapter adapter;
    IActivityService activityService;
    INotificationService notificationService;
    List<ActivityItem> activities;
    NotificationItem tmpItem;
    View select;
    ImageView selectImageView;
    TextView selectTextView;
    EditText editText;

    public EditNotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_notification, container, false);
        hours = 0;
        minutes = 0;
        select = view.findViewById(R.id.activity_select);
        selectImageView = (ImageView) view.findViewById(R.id.icon_activity);
        selectTextView = (TextView) view.findViewById(R.id.name_activity);
        editText = (EditText) view.findViewById(R.id.notification_text);
        activityService = ((NotificationsActivity) getActivity()).getActivityService();
        notificationService = ((NotificationsActivity) getActivity()).getService();
        List<NotificationItem> notifications = notificationService.getAllNotifications();
        List<Long> ids = new ArrayList<>();
        for (NotificationItem notification : notifications) {
            ids.add(notification.activityItem.id);
        }
        activities = activityService.getAllItems();
        List<ActivityItem> tmpActivities = new ArrayList<>();
        boolean used = false;
        for (ActivityItem activity : activities) {
            used = false;
            for (NotificationItem notification : notifications) {
                if (notification.activityItem.id == activity.id) {
                    used = true;
                    break;
                }
            }
            if (!used) {
                tmpActivities.add(activity);
            }
        }
        activities = tmpActivities;
        tmpItem = notificationService.getCurrentItem();
        if (tmpItem == null) {
            isAddingMode = true;
            tmpItem = new NotificationItem(0, activities.get(0), editText.getText().toString());
        } else {
            activities.add(tmpItem.activityItem);
            editText.setText(tmpItem.text);
            hours = (int) tmpItem.limitDay / 3600000;
            minutes = (int) tmpItem.limitDay / 60000 % 60;
        }
        adapter = new ItemAdapter(getActivity(), activities);
        selectTextView.setText(tmpItem.activityItem.name);
        selectImageView.setImageResource(tmpItem.activityItem.icon);
        select.setBackgroundColor(tmpItem.activityItem.background);
        TextView timeText = (TextView) view.findViewById(R.id.time_picker);
        timeText.setText("" + hours + ":" + minutes);
        if (minutes < 10)
            timeText.setText("" + hours + ":0" + minutes);
        timeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = hours;
                int minute = minutes;
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        hours = selectedHour;
                        minutes = selectedMinute;
                        timeText.setText(selectedHour + ":" + selectedMinute);
                        if (minutes < 10)
                            timeText.setText("" + hours + ":0" + minutes);
                        tmpItem.limitDay = hours * 3600000 + minutes * 60000;
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select day limit");
                mTimePicker.show();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertIconDialog();
            }
        });
        FloatingActionButton fbSave = (FloatingActionButton) view.findViewById(R.id.fb_edit_notification_save);
        fbSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tmpItem.text = editText.getText().toString();
                if (isAddingMode) {
                    notificationService.addNotification(tmpItem);
                } else {
                    notificationService.updateNotification(tmpItem);
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.notifications_fragment, new NotificationsFragment());
                ft.commit();
            }
        });
        return view;
    }

    private void showAlertIconDialog() {
        // Prepare grid view
        GridView gridView = new GridView(getActivity());

        gridView.setAdapter(adapter);
        gridView.setNumColumns(3);
        gridView.setHorizontalSpacing(20);
        gridView.setVerticalSpacing(20);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final AlertDialog dialog = builder.create();
        dialog.setView(gridView, 20, 20, 20, 20);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.start_background);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tmpItem.activityItem = activities.get(position);
                selectTextView.setText(tmpItem.activityItem.name);
                selectImageView.setImageResource(tmpItem.activityItem.icon);
                select.setBackgroundColor(tmpItem.activityItem.background);
                dialog.dismiss();
            }
        });
    }
}
