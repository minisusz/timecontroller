package pw.minisusz.timecontroller;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class LocationsSettingsActivity extends AppCompatActivity {

    LocationsSettings settingsScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        settingsScreen = new LocationsSettings();
        getFragmentManager().beginTransaction().replace(android.R.id.content, settingsScreen).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home: {
                onBackPressed();
            }
        }

        return super.onOptionsItemSelected(item);

    }
}
