package pw.minisusz.timecontroller;

/**
 * Created by Lukasz on 2017-05-16.
 */

public class TimeHelper {

    public static String printTime(long miliseconds) {

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = miliseconds / hoursInMilli;
        miliseconds = miliseconds % hoursInMilli;

        long elapsedMinutes = miliseconds / minutesInMilli;
        miliseconds = miliseconds % minutesInMilli;

        long elapsedSeconds = miliseconds / secondsInMilli;

        return String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds);
    }
}
