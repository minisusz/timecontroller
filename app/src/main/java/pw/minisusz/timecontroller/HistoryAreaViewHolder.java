package pw.minisusz.timecontroller;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Lukasz on 2017-05-18.
 */

public class HistoryAreaViewHolder extends RecyclerView.ViewHolder {
    public TextView legendName;
    public TextView legendValue;

    public HistoryAreaViewHolder(View itemView) {
        super(itemView);
        this.legendName = (TextView) itemView.findViewById(R.id.legend_area_textview);
        this.legendValue = (TextView) itemView.findViewById(R.id.legend_area_value);
    }
}
