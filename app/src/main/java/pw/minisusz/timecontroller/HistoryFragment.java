package pw.minisusz.timecontroller;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import pw.minisusz.timecontroller.Services.IHistoryService;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private IHistoryService historyService;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        historyService = ((HistoryActivity) getActivity()).getHistoryService();

        Button showDatePicker = (Button) view.findViewById(R.id.show_datepicker);

        showDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        return view;
    }

    private void showDatePicker() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        dialog.setContentView(R.layout.dialog_date_picker);

        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        datePicker.setMaxDate(System.currentTimeMillis());
        Button btnSave = (Button) dialog.findViewById(R.id.btn_save_date);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyService.setChoosenDate(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                dialog.cancel();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.history_fragment, new HistoryActivityDayFragment());
                ft.addToBackStack("HistoryFragment");
                ft.commit();
            }
        });
        dialog.show();
    }
}
