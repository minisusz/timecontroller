package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public class TimerItem {
    public long id;
    public long startTime;
    public long endTime;
    public ActivityItem activityItem;

    public TimerItem() {
    }

    public TimerItem(long id, long startTime, long endTime, ActivityItem activityItem) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityItem = activityItem;
    }
}
