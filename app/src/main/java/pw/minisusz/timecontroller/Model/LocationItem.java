package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 10.05.2017.
 */

public class LocationItem {
    public long id;
    public long startTime;
    public long endTime;
    public AreaItem areaItem;

    public LocationItem() {
    }

    public LocationItem(long id, long startTime, long endTime, AreaItem areaItem) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.areaItem = areaItem;
    }
}
