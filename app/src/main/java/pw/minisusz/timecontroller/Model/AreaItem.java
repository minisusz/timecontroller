package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 10.05.2017.
 */

public class AreaItem {
    public long id;
    public String name;
    public double latitude;
    public double longitude;
    public boolean available;

    public AreaItem(double latitude, double longitude, String name, boolean available) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.available = available;
    }

    public AreaItem(long id, double latitude, double longitude, String name, boolean available) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.available = available;
    }
}
