package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public class NotificationItem {
    public long id;
    public long limitDay;
    public String text;
    public ActivityItem activityItem;

    public NotificationItem(long id, long limitDay, ActivityItem activityItem, String text) {
        this.id = id;
        this.limitDay = limitDay;
        this.activityItem = activityItem;
        this.text = text;
    }

    public NotificationItem(long limitDay, ActivityItem activityItem, String text) {
        this.limitDay = limitDay;
        this.activityItem = activityItem;
        this.text = text;
    }
}
