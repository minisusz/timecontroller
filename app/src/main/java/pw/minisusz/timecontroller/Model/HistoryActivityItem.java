package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 07.05.2017.
 */

public class HistoryActivityItem {
    private ActivityItem activityItem;
    private long seconds;

    public HistoryActivityItem(ActivityItem activityItem, long seconds) {
        this.activityItem = activityItem;
        this.seconds = seconds;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    public ActivityItem getActivityItem() {
        return activityItem;
    }

    public long getSeconds() {
        return seconds;
    }

    public long getMinutes() {
        return seconds / 60;
    }

    public String getStringTime() {
        long different = seconds;
        long secondsInMilli = 1;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds);
    }
}
