package pw.minisusz.timecontroller.Model;

import android.provider.BaseColumns;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public final class TimerDb {

    private TimerDb() {
    }

    public static class ActivityItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "ActivityItems";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_ICON = "icon";
        public static final String COLUMN_NAME_BACKGROUND = "background";
        public static final String COLUMN_NAME_AVAILABLE = "available";
    }

    public static class TimerItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "TimerItems";
        public static final String COLUMN_NAME_STARTTIME = "startTime";
        public static final String COLUMN_NAME_ENDTIME = "endTime";
        public static final String COLUMN_NAME_IDACTIVITY = "idActivity";
    }

    public static class LocationItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "LocationItems";
        public static final String COLUMN_NAME_STARTTIME = "startTime";
        public static final String COLUMN_NAME_ENDTIME = "endTime";
        public static final String COLUMN_NAME_IDAREA = "idArea";
    }

    public static class AreaItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "AreaItems";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_AVAILABLE = "available";
    }

    public static class NotificationItemsEntry implements BaseColumns {
        public static final String TABLE_NAME = "NotificationItems";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_LIMIT = "limitDay";
        public static final String COLUMN_NAME_IDACTIVITY = "idActivity";
    }
}
