package pw.minisusz.timecontroller.Model;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public class HistoryAreaItem {
    private AreaItem areaItem;
    private long seconds;

    public HistoryAreaItem(AreaItem locationItem, long seconds) {
        this.areaItem = locationItem;
        this.seconds = seconds;
    }

    public long getSeconds() {
        return seconds;
    }

    public long getMinutes() {
        return seconds / 60;
    }

    public void setSeconds(long seconds) {
        this.seconds = seconds;
    }

    public AreaItem getLocationItem() {
        return areaItem;
    }

    public String getStringTime() {
        long different = seconds;
        long secondsInMilli = 1;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return String.format("%02d:%02d:%02d", elapsedHours, elapsedMinutes, elapsedSeconds);
    }
}
