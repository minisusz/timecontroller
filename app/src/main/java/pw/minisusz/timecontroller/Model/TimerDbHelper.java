package pw.minisusz.timecontroller.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;

import pw.minisusz.timecontroller.R;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public class TimerDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 13;

    public static final String DATABASE_NAME = "Timer.db";

    private static final String SQL_CREATE_ActivityItems =
            "CREATE TABLE " + TimerDb.ActivityItemsEntry.TABLE_NAME + " (" +
                    TimerDb.ActivityItemsEntry._ID + " INTEGER PRIMARY KEY," +
                    TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME + " TEXT," +
                    TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON + " INTEGER," +
                    TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND + " INTEGER," +
                    TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE + " INTEGER" +
                    " )";

    private static final String SQL_CREATE_TimerItems =
            "CREATE TABLE " + TimerDb.TimerItemsEntry.TABLE_NAME + " (" +
                    TimerDb.TimerItemsEntry._ID + " INTEGER PRIMARY KEY," +
                    TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME + " INTEGER," +
                    TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME + " INTEGER," +
                    TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY + " INTEGER," +
                    "FOREIGN KEY (" + TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY +
                    ") REFERENCES " + TimerDb.ActivityItemsEntry.TABLE_NAME + "(" +
                    TimerDb.ActivityItemsEntry._ID + ")" +
                    " )";

    private static final String SQL_CREATE_LocationItems =
            "CREATE TABLE " + TimerDb.LocationItemsEntry.TABLE_NAME + " (" +
                    TimerDb.LocationItemsEntry._ID + " INTEGER PRIMARY KEY," +
                    TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME + " INTEGER," +
                    TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME + " INTEGER," +
                    TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA + " INTEGER," +
                    "FOREIGN KEY (" + TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA +
                    ") REFERENCES " + TimerDb.AreaItemsEntry.TABLE_NAME + "(" +
                    TimerDb.AreaItemsEntry._ID + ")" +
                    " )";

    private static final String SQL_CREATE_AreaItems =
            "CREATE TABLE " + TimerDb.AreaItemsEntry.TABLE_NAME + " (" +
                    TimerDb.AreaItemsEntry._ID + " INTEGER PRIMARY KEY," +
                    TimerDb.AreaItemsEntry.COLUMN_NAME_NAME + " TEXT," +
                    TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE + " REAL," +
                    TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE + " REAL," +
                    TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE + " INTEGER" +
                    " )";

    private static final String SQL_CREATE_NotificationItems =
            "CREATE TABLE " + TimerDb.NotificationItemsEntry.TABLE_NAME + " (" +
                    TimerDb.NotificationItemsEntry._ID + " INTEGER PRIMARY KEY," +
                    TimerDb.NotificationItemsEntry.COLUMN_NAME_TEXT + " TEXT," +
                    TimerDb.NotificationItemsEntry.COLUMN_NAME_LIMIT + " INTEGER," +
                    TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY + " INTEGER," +
                    "FOREIGN KEY (" + TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY +
                    ") REFERENCES " + TimerDb.ActivityItemsEntry.TABLE_NAME + "(" +
                    TimerDb.ActivityItemsEntry._ID + ")" +
                    " )";

    private static final String SQL_DELETE_ActivityItems =
            "DROP TABLE IF EXISTS " + TimerDb.ActivityItemsEntry.TABLE_NAME;

    private static final String SQL_DELETE_TimerItems =
            "DROP TABLE IF EXISTS " + TimerDb.TimerItemsEntry.TABLE_NAME;

    private static final String SQL_DELETE_LocationItems =
            "DROP TABLE IF EXISTS " + TimerDb.LocationItemsEntry.TABLE_NAME;

    private static final String SQL_DELETE_AreaItems =
            "DROP TABLE IF EXISTS " + TimerDb.AreaItemsEntry.TABLE_NAME;

    private static final String SQL_DELETE_NotificationItems =
            "DROP TABLE IF EXISTS " + TimerDb.NotificationItemsEntry.TABLE_NAME;

    public TimerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TimerItems);
        db.execSQL(SQL_CREATE_ActivityItems);
        db.execSQL(SQL_CREATE_LocationItems);
        db.execSQL(SQL_CREATE_AreaItems);
        db.execSQL(SQL_CREATE_NotificationItems);
        TimerQueryUtils query = new TimerQueryUtils(db);
        query.addActivityItem(new ActivityItem(Color.parseColor("#283593"), "Sleeping", R.drawable.ic_local_hotel_white_24px, true));
        query.addActivityItem(new ActivityItem(Color.parseColor("#000000"), "Working", R.drawable.ic_work_white_24px, true));
        query.addActivityItem(new ActivityItem(Color.parseColor("#EB1460"), "Play", R.drawable.ic_computer_white_24px, true));
        query.addActivityItem(new ActivityItem(Color.parseColor("#FF9800"), "Running", R.drawable.ic_directions_run_white_24px, true));
        query.addActivityItem(new ActivityItem(Color.parseColor("#46AF4A"), "Cycling", R.drawable.ic_directions_bike_white_24px, true));
        query.addActivityItem(new ActivityItem(Color.parseColor("#9C1AB1"), "Shopping", R.drawable.ic_shopping_cart_white_24px, true));
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ActivityItems);
        db.execSQL(SQL_DELETE_TimerItems);
        db.execSQL(SQL_DELETE_LocationItems);
        db.execSQL(SQL_DELETE_AreaItems);
        db.execSQL(SQL_DELETE_NotificationItems);
        onCreate(db);
    }
}
