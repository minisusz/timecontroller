package pw.minisusz.timecontroller.Model;

/**
 * Created by Lukasz on 2017-04-27.
 */

public class ActivityItem {
    public long id;
    public int icon;
    public String name;
    public int background;
    public boolean available;

    public ActivityItem(long id, int background, String name, int icon, boolean available) {
        this.id = id;
        this.background = background;
        this.name = name;
        this.icon = icon;
        this.available = available;
    }

    public ActivityItem(int background, String name, int icon, boolean available) {
        this.background = background;
        this.name = name;
        this.icon = icon;
        this.available = available;
    }
}
