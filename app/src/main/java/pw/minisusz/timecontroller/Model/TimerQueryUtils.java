package pw.minisusz.timecontroller.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public class TimerQueryUtils {
    private SQLiteDatabase database;

    public TimerQueryUtils(Context context) {
        TimerDbHelper timerDbHelper = new TimerDbHelper(context);
        this.database = timerDbHelper.getWritableDatabase();
    }

    public TimerQueryUtils(SQLiteDatabase database) {
        this.database = database;
    }

    private static String[] projectionActivityItems = {
            TimerDb.ActivityItemsEntry._ID,
            TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME,
            TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON,
            TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND,
            TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE
    };

    private static String[] projectionTimerItems = {
            TimerDb.TimerItemsEntry._ID,
            TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME,
            TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME,
            TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY
    };

    private static String[] projectionLocationItems = {
            TimerDb.LocationItemsEntry._ID,
            TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME,
            TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME,
            TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA
    };

    private static String[] projectionAreaItems = {
            TimerDb.AreaItemsEntry._ID,
            TimerDb.AreaItemsEntry.COLUMN_NAME_NAME,
            TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE,
            TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE,
            TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE
    };

    private static String[] projectionNotificationItems = {
            TimerDb.NotificationItemsEntry._ID,
            TimerDb.NotificationItemsEntry.COLUMN_NAME_TEXT,
            TimerDb.NotificationItemsEntry.COLUMN_NAME_LIMIT,
            TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY
    };

    /////////// QueryUtils TimerItem ////////////

    public TimerItem getLastTimerItem() {
        TimerItem timerItem = null;
        Cursor cursor = database.query(
                TimerDb.TimerItemsEntry.TABLE_NAME,                     // The table to query
                projectionTimerItems,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        if (cursor.moveToLast()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry._ID));
            long startTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME));
            long endTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME));
            long idActivity = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY));
            timerItem = new TimerItem(id, startTime, endTime, getActiveItem(idActivity));
        }

        return timerItem;
    }

    public List<TimerItem> getTimerItemsOnDay(long startDay, long endDay) {
        List<TimerItem> items = new ArrayList<>();
        Cursor cursor = database.query(
                TimerDb.TimerItemsEntry.TABLE_NAME,                     // The table to query
                projectionTimerItems,                               // The columns to return
                "min(endTime, ?) - max(startTime, ?) > 0",                                // The columns for the WHERE clause
                new String[]{Long.toString(endDay), Long.toString(startDay)},                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry._ID));
            long startTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME));
            long endTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME));
            long idActivity = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY));
            items.add(new TimerItem(id, startTime, endTime, getActiveItem(idActivity)));
        }

        return items;
    }

    public void addTimerItem(TimerItem timerItem) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME, timerItem.startTime);
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME, timerItem.endTime);
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY, timerItem.activityItem.id);
        timerItem.id = database.insert(TimerDb.TimerItemsEntry.TABLE_NAME, null, values);
    }

    public void updateTimerItem(TimerItem timerItem) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_STARTTIME, timerItem.startTime);
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_ENDTIME, timerItem.endTime);
        values.put(TimerDb.TimerItemsEntry.COLUMN_NAME_IDACTIVITY, timerItem.activityItem.id);

        String selection = TimerDb.TimerItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", timerItem.id)};

        database.update(
                TimerDb.TimerItemsEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteTimerItem(TimerItem timerItem) {
        String selection = TimerDb.TimerItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", timerItem.id)};
        database.delete(TimerDb.TimerItemsEntry.TABLE_NAME, selection, selectionArgs);
    }

    /////////// QueryUtils ActiveItem ////////////

    public List<ActivityItem> getAllAvailableActivityItems() {
        List<ActivityItem> items = new ArrayList<>();
        Cursor cursor = database.query(
                TimerDb.ActivityItemsEntry.TABLE_NAME,                     // The table to query
                projectionActivityItems,                               // The columns to return
                "available = ?",                                // The columns for the WHERE clause
                new String[]{"1"},                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME));
            int icon = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON));
            int background = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND));
            boolean available = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE)) == 1;
            items.add(new ActivityItem(id, background, name, icon, available));
        }

        return items;
    }

    public ActivityItem getActiveItem(long idActive) {
        ActivityItem activityItem = null;
        Cursor cursor = database.query(
                TimerDb.ActivityItemsEntry.TABLE_NAME,                     // The table to query
                projectionActivityItems,                               // The columns to return
                "_id = ?",                                // The columns for the WHERE clause
                new String[]{Long.toString(idActive)},                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor.moveToFirst()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME));
            int icon = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON));
            int background = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND));
            boolean available = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE)) == 1;
            activityItem = new ActivityItem(id, background, name, icon, available);
        }

        return activityItem;
    }

    public void addActivityItem(ActivityItem item) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME, item.name);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON, item.icon);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND, item.background);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE, item.available);
        item.id = database.insert(TimerDb.ActivityItemsEntry.TABLE_NAME, null, values);
    }

    public void updateActivityItem(ActivityItem item) {
        ContentValues values = new ContentValues();

        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_NAME, item.name);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_ICON, item.icon);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_BACKGROUND, item.background);
        values.put(TimerDb.ActivityItemsEntry.COLUMN_NAME_AVAILABLE, item.available);

        String selection = TimerDb.ActivityItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};

        database.update(
                TimerDb.ActivityItemsEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteActivityItem(ActivityItem item) {
        String selection = TimerDb.ActivityItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};
        database.delete(TimerDb.ActivityItemsEntry.TABLE_NAME, selection, selectionArgs);
    }

    /////////// QueryUtils LocationItem ////////////

    public LocationItem getLastLocationItem() {
        LocationItem LocationItem = null;
        Cursor cursor = database.query(
                TimerDb.LocationItemsEntry.TABLE_NAME,                     // The table to query
                projectionLocationItems,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );
        if (cursor.moveToLast()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry._ID));
            long startTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME));
            long endTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME));
            long idArea = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA));
            LocationItem = new LocationItem(id, startTime, endTime, getAreaItem(idArea));
        }

        return LocationItem;
    }

    public List<LocationItem> getLocationItemsOnDay(long startDay, long endDay) {
        List<LocationItem> items = new ArrayList<>();
        Cursor cursor = database.query(
                TimerDb.LocationItemsEntry.TABLE_NAME,                     // The table to query
                projectionLocationItems,                               // The columns to return
                "min(endTime, ?) - max(startTime, ?) > 0",                                // The columns for the WHERE clause
                new String[]{Long.toString(endDay), Long.toString(startDay)},                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry._ID));
            long startTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME));
            long endTime = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME));
            long idArea = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA));
            items.add(new LocationItem(id, startTime, endTime, getAreaItem(idArea)));
        }

        return items;
    }

    public void addLocationItem(LocationItem locationItem) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME, locationItem.startTime);
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME, locationItem.endTime);
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA, locationItem.areaItem.id);
        locationItem.id = database.insert(TimerDb.LocationItemsEntry.TABLE_NAME, null, values);
    }

    public void updateLocationItem(LocationItem locationItem) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_STARTTIME, locationItem.startTime);
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_ENDTIME, locationItem.endTime);
        values.put(TimerDb.LocationItemsEntry.COLUMN_NAME_IDAREA, locationItem.areaItem.id);

        String selection = TimerDb.LocationItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", locationItem.id)};

        database.update(
                TimerDb.LocationItemsEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteLocationItem(LocationItem locationItem) {
        String selection = TimerDb.LocationItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", locationItem.id)};
        database.delete(TimerDb.LocationItemsEntry.TABLE_NAME, selection, selectionArgs);
    }

    /////////// QueryUtils AreaItem ////////////

    public List<AreaItem> getAvailableAreaItems() {
        List<AreaItem> items = new ArrayList<>();
        Cursor cursor = database.query(
                TimerDb.AreaItemsEntry.TABLE_NAME,                     // The table to query
                projectionAreaItems,                               // The columns to return
                "available = ?",                                // The columns for the WHERE clause
                new String[]{"1"},                          // The columns for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_NAME));
            double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE));
            boolean available = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE)) == 1;
            items.add(new AreaItem(id, latitude, longitude, name, available));
        }

        return items;
    }

    public AreaItem getAreaItem(long idArea) {
        AreaItem areaItem = null;
        Cursor cursor = database.query(
                TimerDb.AreaItemsEntry.TABLE_NAME,                     // The table to query
                projectionAreaItems,                               // The columns to return
                "_id = ?",                                // The columns for the WHERE clause
                new String[]{Long.toString(idArea)},                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor.moveToFirst()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry._ID));
            String name = cursor.getString(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_NAME));
            double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE));
            boolean available = cursor.getInt(cursor.getColumnIndexOrThrow(TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE)) == 1;
            areaItem = new AreaItem(id, latitude, longitude, name, available);
        }

        return areaItem;
    }

    public void addAreaItem(AreaItem item) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_NAME, item.name);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE, item.latitude);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE, item.longitude);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE, item.available);
        item.id = database.insert(TimerDb.AreaItemsEntry.TABLE_NAME, null, values);
    }

    public void updateAreaItem(AreaItem item) {
        ContentValues values = new ContentValues();

        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_NAME, item.name);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_LATITUDE, item.latitude);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_LONGITUDE, item.longitude);
        values.put(TimerDb.AreaItemsEntry.COLUMN_NAME_AVAILABLE, item.available);

        String selection = TimerDb.AreaItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};

        database.update(
                TimerDb.AreaItemsEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteAreaItem(AreaItem item) {
        String selection = TimerDb.AreaItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};
        database.delete(TimerDb.AreaItemsEntry.TABLE_NAME, selection, selectionArgs);
    }

    /////////// QueryUtils AreaItem ////////////

    public List<NotificationItem> getNotificationItems() {
        List<NotificationItem> items = new ArrayList<>();
        Cursor cursor = database.query(
                TimerDb.NotificationItemsEntry.TABLE_NAME,                     // The table to query
                projectionNotificationItems,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                          // The columns for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.NotificationItemsEntry._ID));
            long limit = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.NotificationItemsEntry.COLUMN_NAME_LIMIT));
            long idActivity = cursor.getLong(cursor.getColumnIndexOrThrow(TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY));
            String text = cursor.getString(cursor.getColumnIndexOrThrow(TimerDb.NotificationItemsEntry.COLUMN_NAME_TEXT));
            items.add(new NotificationItem(id, limit, getActiveItem(idActivity), text));
        }

        return items;
    }

    public void addNotificationItem(NotificationItem item) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_TEXT, item.text);
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_LIMIT, item.limitDay);
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY, item.activityItem.id);
        item.id = database.insert(TimerDb.NotificationItemsEntry.TABLE_NAME, null, values);
    }

    public void updateNotificationItem(NotificationItem item) {
        ContentValues values = new ContentValues();
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_TEXT, item.text);
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_LIMIT, item.limitDay);
        values.put(TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY, item.activityItem.id);

        String selection = TimerDb.NotificationItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};

        database.update(
                TimerDb.NotificationItemsEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }

    public void deleteNotificationItem(NotificationItem item) {
        String selection = TimerDb.NotificationItemsEntry._ID + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};
        database.delete(TimerDb.NotificationItemsEntry.TABLE_NAME, selection, selectionArgs);
    }

    public void deleteNotificationItemForActivityItem(ActivityItem item) {
        String selection = TimerDb.NotificationItemsEntry.COLUMN_NAME_IDACTIVITY + " LIKE ?";
        String[] selectionArgs = {String.format("%d", item.id)};
        database.delete(TimerDb.NotificationItemsEntry.TABLE_NAME, selection, selectionArgs);
    }
}
