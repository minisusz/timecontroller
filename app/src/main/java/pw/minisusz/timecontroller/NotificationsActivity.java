package pw.minisusz.timecontroller;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import pw.minisusz.timecontroller.Services.ActivityService;
import pw.minisusz.timecontroller.Services.IActivityService;
import pw.minisusz.timecontroller.Services.INotificationService;
import pw.minisusz.timecontroller.Services.NotificationService;

public class NotificationsActivity extends BaseActivity {

    private INotificationService service;
    private IActivityService activityService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = new NotificationService(getApplication());
        activityService = new ActivityService(getApplication());
        getLayoutInflater().inflate(R.layout.activity_notifications, frameLayout);
        setTitle("Notifications");
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.notifications_fragment, new NotificationsFragment());
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(3).setChecked(true);
    }

    public INotificationService getService() {
        return service;
    }

    public IActivityService getActivityService() {
        return activityService;
    }
}
