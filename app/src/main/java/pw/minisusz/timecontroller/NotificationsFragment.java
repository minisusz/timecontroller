package pw.minisusz.timecontroller;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import pw.minisusz.timecontroller.Adapters.NotificationAdapter;
import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.Services.IActivityService;
import pw.minisusz.timecontroller.Services.INotificationService;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {

    List<NotificationItem> notifications;
    INotificationService service;
    NotificationAdapter adapter;
    IActivityService activityService;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        service = ((NotificationsActivity) getActivity()).getService();
        activityService = ((NotificationsActivity) getActivity()).getActivityService();
        notifications = service.getAllNotifications();
        ListView listView = (ListView) view.findViewById(R.id.notifications_list);
        adapter = new NotificationAdapter(getActivity(), notifications);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deletingItemDialog(position);
                return true;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                service.setCurrentItem(notifications.get(position));
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.notifications_fragment, new EditNotificationFragment());
                ft.addToBackStack("NotificationsFragment");
                ft.commit();
            }
        });
        FloatingActionButton fbAdd = (FloatingActionButton) view.findViewById(R.id.fb_add_notification);
        fbAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getCount() >= activityService.getAllItems().size()) {
                    Toast.makeText(getActivity(), "All activities have notification", Toast.LENGTH_SHORT).show();
                } else {
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.notifications_fragment, new EditNotificationFragment());
                    ft.addToBackStack("NotificationsFragment");
                    ft.commit();
                }
            }
        });
        return view;
    }

    private void deletingItemDialog(int position) {
        service.setCurrentItem(notifications.get(position));
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you want to delete notification on " + notifications.get(position).activityItem.name + " activity?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        service.deleteNotification();
                        notifications.remove(position);
                        adapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
