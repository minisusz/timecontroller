package pw.minisusz.timecontroller;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by RAPTOR on 07.05.2017.
 */

public class HistoryItemViewHolder extends RecyclerView.ViewHolder {
    public TextView legendName;
    public TextView legendValue;
    public View viewIcon;
    public ImageView legendIcon;
    public TextView legendNameIcon;

    public HistoryItemViewHolder(View itemView) {
        super(itemView);
        this.legendName = (TextView) itemView.findViewById(R.id.legend_name_textview);
        this.legendValue = (TextView) itemView.findViewById(R.id.legend_value_textview);
        this.viewIcon = itemView.findViewById(R.id.legend_activity);
        this.legendIcon = (ImageView) itemView.findViewById(R.id.legend_icon);
        this.legendNameIcon = (TextView) itemView.findViewById(R.id.legend_name_icon);
    }
}
