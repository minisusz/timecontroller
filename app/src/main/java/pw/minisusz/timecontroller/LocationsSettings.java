package pw.minisusz.timecontroller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AlertDialog;

import pw.minisusz.timecontroller.Services.GpsService;

/**
 * Created by Lukasz on 2017-05-22.
 */

public class LocationsSettings extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static String keyDetecting = "switch_detecting";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.location_settings_screen);

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(keyDetecting)) {
            boolean isChecked = sharedPreferences.getBoolean("switch_detecting", false);

            if (isChecked) {
                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                WifiManager wifi = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && (!wifi.isWifiEnabled() ||
                        (wifi.isWifiEnabled() && wifi.getConnectionInfo().getNetworkId() == -1))) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    buildAlertMessageNoGps();
                    editor.putBoolean("switch_detecting", false);
                    editor.apply();
                    setPreferenceScreen(null);
                    addPreferencesFromResource(R.xml.location_settings_screen);
                    return;
                }
                Intent intent = new Intent(getActivity().getApplicationContext(), GpsService.class);
                getActivity().getApplicationContext().startService(intent);
            } else {
                Intent intent = new Intent(getActivity().getApplicationContext(), GpsService.class);
                getActivity().getApplicationContext().stopService(intent);
            }

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
