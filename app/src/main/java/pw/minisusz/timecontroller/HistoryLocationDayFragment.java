package pw.minisusz.timecontroller;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.List;

import pw.minisusz.timecontroller.Adapters.LegendAreaAdapter;
import pw.minisusz.timecontroller.Model.HistoryAreaItem;
import pw.minisusz.timecontroller.Services.IHistoryService;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public class HistoryLocationDayFragment extends Fragment {
    private IHistoryService historyService;
    private GraphView graph;
    private RecyclerView recyclerView;
    private LegendAreaAdapter legendAdapter;

    // newInstance constructor for creating fragment with arguments
    public static HistoryLocationDayFragment newInstance(int page, String title) {
        HistoryLocationDayFragment fragmentLocation = new HistoryLocationDayFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentLocation.setArguments(args);
        return fragmentLocation;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_location_day, container, false);
        historyService = ((HistoryActivity) getActivity()).getHistoryService();

        graph = (GraphView) view.findViewById(R.id.graph_location);
        List<HistoryAreaItem> historyActivityItemList = historyService.getListHistoryAreaItems();
        int n = historyActivityItemList.size();
        if (n == 0) {
            return view;
        }
        DataPoint[] dataPoints = new DataPoint[n + 1];
        for (int i = 0; i < n; i++) {
            dataPoints[i] = new DataPoint(i + 1, historyActivityItemList.get(i).getMinutes());
        }
        dataPoints[n] = new DataPoint(n + 1, 0);
        graph.addSeries(getSeries(dataPoints));
        setGraph(n);

        recyclerView = (RecyclerView) view.findViewById(R.id.legend_areas_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        legendAdapter = new LegendAreaAdapter(historyActivityItemList);
        recyclerView.setAdapter(legendAdapter);


        return view;
    }

    private BarGraphSeries<DataPoint> getSeries(DataPoint[] dataPoints) {
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(dataPoints);
        series.setAnimated(true);
        series.setSpacing(50);
        series.setColor(Color.rgb((int) dataPoints[0].getX() * 255 / 4, (int) Math.abs(dataPoints[0].getY() * 255 / 6), 100));
        return series;
    }

    private void setGraph(int range) {
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(range + 2);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.setTitle(historyService.getStringChoosenDate());
        graph.getGridLabelRenderer().setVerticalAxisTitle("[min]");
        graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
        graph.getViewport().setScrollable(true);
    }
}
