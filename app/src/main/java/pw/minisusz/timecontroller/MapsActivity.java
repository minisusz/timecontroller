package pw.minisusz.timecontroller;

import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import pw.minisusz.timecontroller.Model.AreaItem;
import pw.minisusz.timecontroller.Services.AreaService;
import pw.minisusz.timecontroller.Services.IAreaService;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap mMap;
    private IAreaService areaService;
    private LocationManager locationManager;
    private List<AreaItem> areas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        areaService = new AreaService(getApplicationContext());
        areas = areaService.getAllAreas();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (!areaService.isOnArea(latLng))
                    addMarker(latLng);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.getTitle().equals("My position")) {
                    if (!areaService.isOnArea(marker.getPosition()))
                        addMarker(marker.getPosition());
                }
                return false;
            }
        });

        LocationListener locationListener = new LocationListener() {
            private boolean firstTime = true;

            @Override
            public void onLocationChanged(Location location) {
                LatLng myPosition = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.clear();
                writeAreas();
                mMap.addMarker(new MarkerOptions().position(myPosition).title("My position"));
                if (firstTime) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 14.0f));
                    firstTime = false;
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        writeAreas();

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private void addMarker(LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter the name position");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        builder.setView(input);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text;
                m_Text = input.getText().toString();
                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
                AreaItem newItem = new AreaItem(latLng.latitude, latLng.longitude, m_Text, true);
                areaService.addAreaItem(newItem);
                areas.add(newItem);
                writeArea(newItem);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void writeArea(AreaItem area) {
        LatLng position = new LatLng(area.latitude, area.longitude);
        mMap.addMarker(new MarkerOptions().position(position).title(area.name)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(position);
        circleOptions.radius(150);
        circleOptions.fillColor(Color.argb(124, 50, 100, 200));
        mMap.addCircle(circleOptions);
    }

    private void writeAreas() {
        for (AreaItem area : areas) {
            writeArea(area);
        }
    }
}
