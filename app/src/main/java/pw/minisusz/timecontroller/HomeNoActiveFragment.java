package pw.minisusz.timecontroller;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import java.util.Calendar;
import java.util.List;

import pw.minisusz.timecontroller.Adapters.ItemAdapter;
import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Services.IHomeService;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public class HomeNoActiveFragment extends Fragment {
    private Button startButton;
    private Spinner activitySpinner;
    private IHomeService timerService;
    private List<ActivityItem> listActivities;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_noactive, container, false);
        timerService = ((HomeActivity) getActivity()).getHomeService();
        startButton = (Button) view.findViewById(R.id.start_button);
        listActivities = timerService.getActivities();
        if (listActivities.size() == 0)
            startButton.setEnabled(false);
        activitySpinner = (Spinner) view.findViewById(R.id.activity_spinner);
        ItemAdapter itemAdapter = new ItemAdapter(getActivity(), listActivities);
        activitySpinner.setAdapter(itemAdapter);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long currTime = Calendar.getInstance().getTimeInMillis();
                ActivityItem activityItem = listActivities.get(activitySpinner.getSelectedItemPosition());
                AlarmReceiver alarmReceiver = new AlarmReceiver();
                alarmReceiver.setAlarm(getActivity(), timerService.limitDayForActivity(activityItem), timerService.getNotificationTextForActivity(activityItem));
                timerService.setStartTime(currTime, activityItem);
                updateWidget();
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_home, new HomeActiveFragment());
                ft.commit();
            }
        });

        return view;
    }

    private void updateWidget() {
        Intent intent = new Intent(getActivity(), TimerWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getActivity());
        ComponentName thisWidget = new ComponentName(getActivity(), TimerWidgetProvider.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        getActivity().sendBroadcast(intent);
    }
}
