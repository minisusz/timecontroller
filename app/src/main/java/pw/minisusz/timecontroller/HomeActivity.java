package pw.minisusz.timecontroller;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import pw.minisusz.timecontroller.Services.HomeService;
import pw.minisusz.timecontroller.Services.IHomeService;

/**
 * Created by RAPTOR on 23.04.2017.
 */

public class HomeActivity extends BaseActivity {
    private IHomeService homeService;

    public IHomeService getHomeService() {
        return homeService;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);
        setTitle("Home");
        homeService = new HomeService(getApplication());
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        IHomeService homeService = new HomeService(getApplicationContext());
        if (homeService.isActive())
            ft.replace(R.id.fragment_home, new HomeActiveFragment());
        else
            ft.replace(R.id.fragment_home, new HomeNoActiveFragment());
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true);
    }
}
