package pw.minisusz.timecontroller.Services;

import android.content.Context;

import java.util.Calendar;
import java.util.List;

import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.Model.TimerItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by RAPTOR on 05.05.2017.
 */

public class HomeService implements IHomeService {
    private TimerItem timerItem;
    private TimerQueryUtils timerQueryUtils;
    private boolean active;
    private long miliInDay = 86400000;

    public HomeService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
        updateActive();
    }

    private void updateActive() {
        timerItem = timerQueryUtils.getLastTimerItem();
        if (timerItem != null) {
            if (timerItem.endTime > 0)
                active = false;
            else
                active = true;
        } else {
            active = false;
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public long getStartTime() {
        return timerItem.startTime;
    }

    @Override
    public void setStartTime(long startTime, ActivityItem activity) {
        timerItem = new TimerItem();
        timerItem.startTime = startTime;
        timerItem.activityItem = activity;
        timerQueryUtils.addTimerItem(timerItem);
        active = true;
    }

    @Override
    public void setEndTime(long endTime) {
        timerItem.endTime = endTime;
        timerQueryUtils.updateTimerItem(timerItem);
        active = false;
    }

    @Override
    public List<ActivityItem> getActivities() {
        return timerQueryUtils.getAllAvailableActivityItems();
    }

    @Override
    public ActivityItem getActiveItem() {
        return timerItem.activityItem;
    }

    @Override
    public void cancel() {
        timerQueryUtils.deleteTimerItem(timerItem);
    }

    @Override
    public void updateHome() {
        updateActive();
    }

    @Override
    public long limitDayForActivity(ActivityItem activityItem) {
        long limitDay = getLimitDay(activityItem);
        if (limitDay <= 0)
            return limitDay;
        long usedTimeDay = 0;
        long startDay, currentDay, endDay;
        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        currentDay = cal.getTimeInMillis();
        cal.clear();
        cal.set(cal2.get(Calendar.YEAR), cal2.get(Calendar.MONTH), cal2.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        startDay = cal.getTimeInMillis();
        endDay = startDay + miliInDay;

        List<TimerItem> list = timerQueryUtils.getTimerItemsOnDay(startDay, endDay);
        for (TimerItem item : list) {
            long addTime = Math.min(endDay, item.endTime) - Math.max(startDay, item.startTime);
            Long id = item.activityItem.id;
            if (addTime > 0 && id == activityItem.id) {
                usedTimeDay += addTime;
            }

        }

        if (endDay - currentDay < limitDay - usedTimeDay || limitDay - usedTimeDay < 0)
            return endDay - currentDay + limitDay;
        else
            return limitDay - usedTimeDay;
    }

    @Override
    public String getNotificationTextForActivity(ActivityItem item) {
        List<NotificationItem> notificationItems = timerQueryUtils.getNotificationItems();
        for (NotificationItem notificationItem : notificationItems) {
            if (notificationItem.activityItem.id == item.id) {
                return notificationItem.text;
            }
        }
        return "";
    }

    private long getLimitDay(ActivityItem activityItem) {
        List<NotificationItem> notificationItems = timerQueryUtils.getNotificationItems();
        long limitDay = 0;

        for (NotificationItem notificationItem : notificationItems) {
            if (notificationItem.activityItem.id == activityItem.id) {
                limitDay = notificationItem.limitDay;
                break;
            }
        }

        return limitDay;
    }
}
