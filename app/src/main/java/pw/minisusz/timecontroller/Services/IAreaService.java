package pw.minisusz.timecontroller.Services;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import pw.minisusz.timecontroller.Model.AreaItem;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public interface IAreaService {

    void addAreaItem(AreaItem areaItem);

    void updateAreaItem(AreaItem newAreaItem);

    void deleteAreaItem(AreaItem areaItem);

    String getNameCurrentArea();

    List<AreaItem> getAllAreas();

    boolean isOnArea(LatLng position);
}
