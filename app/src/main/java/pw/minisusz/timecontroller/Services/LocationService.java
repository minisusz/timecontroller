package pw.minisusz.timecontroller.Services;

import android.content.Context;
import android.location.Location;

import java.util.Calendar;
import java.util.List;

import pw.minisusz.timecontroller.Model.AreaItem;
import pw.minisusz.timecontroller.Model.LocationItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public class LocationService implements ILocationService {
    private TimerQueryUtils timerQueryUtils;
    private boolean startLocation;
    private List<AreaItem> areaItems;
    private AreaItem currentAreaItem;
    private boolean areaChange;

    public LocationService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
        LocationItem locationItem = timerQueryUtils.getLastLocationItem();
        if (locationItem != null && locationItem.endTime <= 0)
            timerQueryUtils.deleteLocationItem(locationItem);
        startLocation = true;
        areaItems = timerQueryUtils.getAvailableAreaItems();
    }

    @Override
    public void currentLocation(Location location) {
        AreaItem areaItem = getAreaItem(location);
        if (!startLocation) {
            if (areaItem != null && currentAreaItem.id == areaItem.id) {
                areaChange = false;
                return;
            }
            LocationItem lastLocationItem = timerQueryUtils.getLastLocationItem();
            lastLocationItem.endTime = Calendar.getInstance().getTimeInMillis();
            timerQueryUtils.updateLocationItem(lastLocationItem);
        }
        if (areaItem == null) {
            startLocation = true;
            if (currentAreaItem != null) {
                currentAreaItem = null;
                areaChange = true;
            } else {
                areaChange = false;
            }
            return;
        }
        LocationItem locationItem = new LocationItem();
        locationItem.startTime = Calendar.getInstance().getTimeInMillis();
        locationItem.areaItem = areaItem;
        currentAreaItem = areaItem;
        startLocation = false;
        areaChange = true;
        timerQueryUtils.addLocationItem(locationItem);
    }

    @Override
    public void stopLocation() {
        if (!startLocation) {
            LocationItem lastLocationItem = timerQueryUtils.getLastLocationItem();
            lastLocationItem.endTime = Calendar.getInstance().getTimeInMillis();
            timerQueryUtils.updateLocationItem(lastLocationItem);
            currentAreaItem = null;
            areaChange = true;
            return;
        }
        areaChange = false;
    }

    @Override
    public String getCurrentNameAreaItem() {
        if (currentAreaItem == null)
            return "unknown";
        else
            return currentAreaItem.name;
    }

    @Override
    public boolean isAreaChange() {
        return areaChange;
    }

    private AreaItem getAreaItem(Location locationA) {
        AreaItem areaItem = null;
        for (AreaItem area : areaItems) {
            Location locationB = new Location("B");
            locationB.setLatitude(area.latitude);
            locationB.setLongitude(area.longitude);
            if (locationA.distanceTo(locationB) < 150) {
                areaItem = area;
            }
        }

        return areaItem;
    }
}
