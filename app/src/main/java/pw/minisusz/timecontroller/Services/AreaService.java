package pw.minisusz.timecontroller.Services;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import pw.minisusz.timecontroller.Model.AreaItem;
import pw.minisusz.timecontroller.Model.LocationItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by RAPTOR on 13.05.2017.
 */

public class AreaService implements IAreaService {

    private TimerQueryUtils timerQueryUtils;


    public AreaService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
    }

    @Override
    public void addAreaItem(AreaItem areaItem) {
        timerQueryUtils.addAreaItem(areaItem);
    }

    @Override
    public void updateAreaItem(AreaItem newAreaItem) {
        timerQueryUtils.updateAreaItem(newAreaItem);
    }

    @Override
    public void deleteAreaItem(AreaItem areaItem) {
        areaItem.available = false;
        timerQueryUtils.updateAreaItem(areaItem);
    }

    @Override
    public String getNameCurrentArea() {
        String name;
        LocationItem locationItem = timerQueryUtils.getLastLocationItem();
        if (locationItem == null || locationItem.endTime > 0)
            name = "unknown";
        else
            name = locationItem.areaItem.name;

        return name;
    }

    @Override
    public List<AreaItem> getAllAreas() {
        return timerQueryUtils.getAvailableAreaItems();
    }

    @Override
    public boolean isOnArea(LatLng position) {
        List<AreaItem> areas = timerQueryUtils.getAvailableAreaItems();
        Location locationA = new Location("A");
        locationA.setLatitude(position.latitude);
        locationA.setLongitude(position.longitude);
        for (AreaItem area : areas) {
            Location locationB = new Location("B");
            locationB.setLatitude(area.latitude);
            locationB.setLongitude(area.longitude);
            if (locationA.distanceTo(locationB) < 150) {
                return true;
            }
        }
        return false;
    }
}
