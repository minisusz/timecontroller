package pw.minisusz.timecontroller.Services;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pw.minisusz.timecontroller.Model.HistoryActivityItem;
import pw.minisusz.timecontroller.Model.HistoryAreaItem;
import pw.minisusz.timecontroller.Model.LocationItem;
import pw.minisusz.timecontroller.Model.TimerItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by RAPTOR on 07.05.2017.
 */

public class HistoryService implements IHistoryService {
    private TimerQueryUtils timerQueryUtils;
    private long startDateHistory;
    private int yearHistory;
    private int monthHistory;
    private int dayHistory;
    private long endDateHistory;
    private long miliInDay = 86400000;

    public HistoryService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
    }


    @Override
    public void setChoosenDate(int year, int month, int day) {
        yearHistory = year;
        monthHistory = month;
        dayHistory = day;
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month, day, 0, 0, 0);

        startDateHistory = cal.getTimeInMillis();
        endDateHistory = startDateHistory + miliInDay;
    }

    @Override
    public String getStringChoosenDate() {
        return String.format("%d-%d-%d", dayHistory, monthHistory + 1, yearHistory);
    }

    @Override
    public List<HistoryActivityItem> getListHistoryActivityItems() {
        List<TimerItem> list = timerQueryUtils.getTimerItemsOnDay(startDateHistory, endDateHistory);
        Map<Long, HistoryActivityItem> map = new HashMap<>();
        List<HistoryActivityItem> listHistoryActivityItem = new ArrayList<>();

        for (TimerItem item : list) {
            long addTime = Math.min(endDateHistory, item.endTime) - Math.max(startDateHistory, item.startTime);
            addTime = addTime / 1000;
            Long id = item.activityItem.id;
            if (addTime > 0) {
                if (map.containsKey(id)) {
                    HistoryActivityItem value = map.get((Long) id);
                    value.setSeconds(value.getSeconds() + addTime);
                } else {
                    map.put(id, new HistoryActivityItem(item.activityItem, addTime));
                }
            }
        }

        for (Map.Entry<Long, HistoryActivityItem> entry : map.entrySet()) {
            HistoryActivityItem value = entry.getValue();

            listHistoryActivityItem.add(value);
        }

        return listHistoryActivityItem;
    }

    @Override
    public List<HistoryAreaItem> getListHistoryAreaItems() {
        List<LocationItem> list = timerQueryUtils.getLocationItemsOnDay(startDateHistory, endDateHistory);
        Map<Long, HistoryAreaItem> map = new HashMap<>();
        List<HistoryAreaItem> historyAreaItems = new ArrayList<>();

        for (LocationItem item : list) {
            long addTime = Math.min(endDateHistory, item.endTime) - Math.max(startDateHistory, item.startTime);
            addTime = addTime / 1000;
            Long id = item.areaItem.id;
            if (addTime > 0) {
                if (map.containsKey(id)) {
                    HistoryAreaItem value = map.get((Long) id);
                    value.setSeconds(value.getSeconds() + addTime);
                } else {
                    map.put(id, new HistoryAreaItem(item.areaItem, addTime));
                }
            }
        }

        for (Map.Entry<Long, HistoryAreaItem> entry : map.entrySet()) {
            HistoryAreaItem value = entry.getValue();

            historyAreaItems.add(value);
        }

        return historyAreaItems;
    }
}
