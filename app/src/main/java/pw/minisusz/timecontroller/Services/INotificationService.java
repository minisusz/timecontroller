package pw.minisusz.timecontroller.Services;

import java.util.List;

import pw.minisusz.timecontroller.Model.NotificationItem;

/**
 * Created by Lukasz on 2017-05-16.
 */

public interface INotificationService {

    List<NotificationItem> getAllNotifications();

    NotificationItem getCurrentItem();

    void setCurrentItem(NotificationItem item);

    void addNotification(NotificationItem newItem);

    void updateNotification(NotificationItem updateItem);

    void deleteNotification();
}
