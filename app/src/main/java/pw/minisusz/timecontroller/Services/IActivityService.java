package pw.minisusz.timecontroller.Services;

import java.util.List;

import pw.minisusz.timecontroller.Model.ActivityItem;

/**
 * Created by RAPTOR on 06.05.2017.
 */

public interface IActivityService {
    void setCurrentItem(ActivityItem item);

    ActivityItem getCurrentItem();

    void addItem(ActivityItem item);

    void updateItem(ActivityItem item);

    void deleteItem();

    List<ActivityItem> getAllItems();
}
