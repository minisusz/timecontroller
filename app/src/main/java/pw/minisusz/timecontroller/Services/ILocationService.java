package pw.minisusz.timecontroller.Services;

import android.location.Location;


/**
 * Created by RAPTOR on 13.05.2017.
 */

public interface ILocationService {

    void currentLocation(Location location);

    void stopLocation();

    String getCurrentNameAreaItem();

    boolean isAreaChange();
}
