package pw.minisusz.timecontroller.Services;

import java.util.List;

import pw.minisusz.timecontroller.Model.HistoryActivityItem;
import pw.minisusz.timecontroller.Model.HistoryAreaItem;

/**
 * Created by RAPTOR on 07.05.2017.
 */

public interface IHistoryService {
    void setChoosenDate(int year, int month, int day);

    String getStringChoosenDate();

    List<HistoryActivityItem> getListHistoryActivityItems();

    List<HistoryAreaItem> getListHistoryAreaItems();
}
