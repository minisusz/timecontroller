package pw.minisusz.timecontroller.Services;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import pw.minisusz.timecontroller.TimerWidgetProvider;

/**
 * Created by RAPTOR on 12.05.2017.
 */

public class GpsService extends Service {

    private LocationListener locationListener;
    private LocationManager locationManager;
    private ILocationService locationService;
    private BroadcastReceiver broadcastReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locationService = new LocationService(getApplicationContext());
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                locationService.currentLocation(location);
                sendUpdateBroadcast();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                checkState();
            }
        };
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 2000.0f, locationListener);//
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                checkState();
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationService.stopLocation();
        sendUpdateBroadcast();
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
        if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("switch_detecting", false)) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
            editor.putBoolean("switch_detecting", false);
            editor.apply();
        }
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    private void checkState() {
        final LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && (!wifi.isWifiEnabled() ||
                (wifi.isWifiEnabled() && wifi.getConnectionInfo().getNetworkId() == -1))) {
            stopSelf();
        }
    }

    private void sendUpdateBroadcast() {
        if (!locationService.isAreaChange())
            return;
        Intent intent = new Intent("LOCATION_UPDATE");
        intent.putExtra("nameArea", locationService.getCurrentNameAreaItem());
        sendBroadcast(intent);
        updateWidget();
    }

    private void updateWidget() {
        Intent intent = new Intent(getApplicationContext(), TimerWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
        ComponentName thisWidget = new ComponentName(getApplicationContext(), TimerWidgetProvider.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        getApplicationContext().sendBroadcast(intent);
    }
}
