package pw.minisusz.timecontroller.Services;

import android.content.Context;

import java.util.List;

import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by Lukasz on 2017-05-16.
 */

public class NotificationService implements INotificationService {

    private TimerQueryUtils timerQueryUtils;
    private NotificationItem currentItem;

    public NotificationService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
    }

    public NotificationItem getCurrentItem() {
        if (currentItem != null)
            return new NotificationItem(currentItem.id, currentItem.limitDay, currentItem.activityItem, currentItem.text);
        return null;
    }

    @Override
    public void setCurrentItem(NotificationItem item) {
        currentItem = item;
    }

    @Override
    public void addNotification(NotificationItem newItem) {
        timerQueryUtils.addNotificationItem(newItem);
        currentItem = null;
    }

    @Override
    public void updateNotification(NotificationItem updateItem) {
        timerQueryUtils.updateNotificationItem(updateItem);
        currentItem = null;
    }

    @Override
    public void deleteNotification() {
        timerQueryUtils.deleteNotificationItem(currentItem);
        currentItem = null;
    }

    @Override
    public List<NotificationItem> getAllNotifications() {
        return timerQueryUtils.getNotificationItems();
    }
}
