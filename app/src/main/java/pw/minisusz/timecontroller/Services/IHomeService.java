package pw.minisusz.timecontroller.Services;

import java.util.List;

import pw.minisusz.timecontroller.Model.ActivityItem;

/**
 * Created by RAPTOR on 05.05.2017.
 */

public interface IHomeService {

    boolean isActive();

    long getStartTime();

    void setStartTime(long startTime, ActivityItem activity);

    void setEndTime(long endTime);

    List<ActivityItem> getActivities();

    ActivityItem getActiveItem();

    void cancel();

    void updateHome();

    long limitDayForActivity(ActivityItem activityItem);

    String getNotificationTextForActivity(ActivityItem item);
}
