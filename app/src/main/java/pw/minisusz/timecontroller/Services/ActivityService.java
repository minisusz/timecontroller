package pw.minisusz.timecontroller.Services;

import android.content.Context;

import java.util.List;

import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Model.NotificationItem;
import pw.minisusz.timecontroller.Model.TimerQueryUtils;

/**
 * Created by Lukasz on 2017-05-06.
 */

public class ActivityService implements IActivityService {

    private ActivityItem currentItem;
    private TimerQueryUtils timerQueryUtils;

    public ActivityService(Context context) {
        timerQueryUtils = new TimerQueryUtils(context);
    }

    @Override
    public void setCurrentItem(ActivityItem item) {
        currentItem = item;
    }

    @Override
    public ActivityItem getCurrentItem() {
        if (currentItem != null)
            return new ActivityItem(currentItem.id, currentItem.background, currentItem.name,
                    currentItem.icon, currentItem.available);
        return null;
    }

    @Override
    public void addItem(ActivityItem item) {
        timerQueryUtils.addActivityItem(item);
        currentItem = null;
    }

    @Override
    public void updateItem(ActivityItem item) {
        if (item.name.equalsIgnoreCase(currentItem.name)) {
            timerQueryUtils.updateActivityItem(item);
        } else {
            currentItem.available = false;
            timerQueryUtils.updateActivityItem(currentItem);
            timerQueryUtils.addActivityItem(item);
        }
        currentItem = null;
    }

    @Override
    public void deleteItem() {
        if (currentItem == null) return;
        currentItem.available = false;
        timerQueryUtils.updateActivityItem(currentItem);
        timerQueryUtils.deleteNotificationItemForActivityItem(currentItem);
        currentItem = null;
    }

    @Override
    public List<ActivityItem> getAllItems() {
        return timerQueryUtils.getAllAvailableActivityItems();
    }
}
