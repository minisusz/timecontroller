package pw.minisusz.timecontroller;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import pw.minisusz.timecontroller.Adapters.AreaAdapter;
import pw.minisusz.timecontroller.Model.AreaItem;
import pw.minisusz.timecontroller.Services.IAreaService;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationsFragment extends Fragment {

    private FloatingActionButton fbAdd;
    private IAreaService areaService;
    private ListView areasListView;
    private AreaAdapter adapter;
    private List<AreaItem> areas;

    public LocationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_locations, container, false);

        areaService = ((LocationsActivity) getActivity()).getAreaService();
        fbAdd = (FloatingActionButton) view.findViewById(R.id.fb_add_location);
        areasListView = (ListView) view.findViewById(R.id.locations_list);
        areas = areaService.getAllAreas();
        adapter = new AreaAdapter(getActivity(), areas);
        areasListView.setAdapter(adapter);
        fbAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), MapsActivity.class);
                startActivity(intent);
            }
        });
        areasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editNameArea(areas.get(position));
            }
        });
        areasListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deletingAreaDialog(position);
                return true;
            }
        });

        if (PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()).getBoolean("switch_detecting", false)) {
            disableButtons();
        } else enableButtons();

        checkPermission();
        return view;
    }

    private void checkPermission() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                    return;
                }
            }
        }
    }

    @Override
    public void onResume() {
        areas = areaService.getAllAreas();
        adapter = new AreaAdapter(getActivity(), areas);
        areasListView.setAdapter(adapter);
        super.onResume();
    }

    private void disableButtons() {
        fbAdd.setEnabled(false);
        fbAdd.setVisibility(View.INVISIBLE);
    }

    private void enableButtons() {
        fbAdd.setEnabled(true);
        fbAdd.setVisibility(View.VISIBLE);
    }

    private void editNameArea(AreaItem areaItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Enter the name position");

        final EditText input = new EditText(getActivity());

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(areaItem.name);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
        builder.setView(input);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text;
                m_Text = input.getText().toString();
                if (m_Text.isEmpty()) {
                    Toast.makeText(getActivity().getApplicationContext(), "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                areaItem.name = m_Text;
                areaService.updateAreaItem(areaItem);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity().getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void deletingAreaDialog(int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you want to delete " + areas.get(position).name + " location?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        areaService.deleteAreaItem(areas.get(position));
                        areas.remove(position);
                        adapter.notifyDataSetChanged();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
