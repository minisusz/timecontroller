package pw.minisusz.timecontroller;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.colorpicker.ColorPickerPalette;
import com.android.colorpicker.ColorPickerSwatch;

import pw.minisusz.timecontroller.Adapters.IconAdapter;
import pw.minisusz.timecontroller.Model.ActivityItem;
import pw.minisusz.timecontroller.Services.IActivityService;


public class EditActivityFragment extends Fragment {

    ImageView selectedIcon;
    int[] colors;
    ColorPickerPalette colorPickerPalette;
    Button button;
    EditText editText;
    ActivityItem tmpItem;
    boolean isAddingMode;
    IActivityService service;
    int[] icons;
    IconAdapter iconAdapter;
    Integer[] resIds;
    AlertDialog alert;

    public EditActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_activity, container, false);
        FloatingActionButton fbSave = (FloatingActionButton) view.findViewById(R.id.fb_edit_activity_save);
        initialize(view);
        button.setBackgroundColor(tmpItem.background);
        editText.setText(tmpItem.name);
        selectedIcon.setImageResource(tmpItem.icon);
        selectedIcon.setBackgroundColor(tmpItem.background);
        iconAdapter.setColor(tmpItem.background);
        colorPickerPalette.init(colors.length, 3, new ColorPickerSwatch.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                colorPickerPalette.drawPalette(colors, color);
                tmpItem.background = color;
            }
        });
        colorPickerPalette.drawPalette(colors, tmpItem.background);
        alert = createAlertColorDialog();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.show();
            }
        });
        selectedIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertIconDialog();
            }
        });
        fbSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tmpItem.name = editText.getText().toString();
                if (tmpItem.name.isEmpty()) {
                    Toast.makeText(getActivity(), "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (isAddingMode) {
                    service.addItem(tmpItem);
                } else
                    service.updateItem(tmpItem);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.activity_fragment, new ActivitiesFragment());
                ft.commit();
            }
        });
        return view;
    }

    private void initialize(View view) {
        button = (Button) view.findViewById(R.id.btn_palette);
        editText = (EditText) view.findViewById(R.id.edit_activity_name);
        selectedIcon = (ImageView) view.findViewById(R.id.imageview_icon);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        colorPickerPalette = (ColorPickerPalette) layoutInflater
                .inflate(R.layout.color_picker_palette, null);
        colors = getResources().getIntArray(R.array.colors);
        service = ((ActivitiesActivity) getActivity()).getService();
        tmpItem = service.getCurrentItem();
        if (tmpItem == null) {
            isAddingMode = true;
            tmpItem = new ActivityItem(colors[0], "", R.drawable.ic_local_play_white_24px, true);
        }
        icons = getResources().getIntArray(R.array.activities_icons);
        resIds = new Integer[icons.length];
        TypedArray images = getResources().obtainTypedArray(R.array.activities_icons);
        for (int i = 0; i < icons.length; i++)
            resIds[i] = images.getResourceId(i, 0);
        iconAdapter = new IconAdapter(getActivity(), resIds);
    }

    private AlertDialog createAlertColorDialog() {
        return new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme)
                .setTitle("Choose color")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        button.setBackgroundColor(tmpItem.background);
                        selectedIcon.setBackgroundColor(tmpItem.background);
                        iconAdapter.setColor(tmpItem.background);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setView(colorPickerPalette)
                .create();
    }

    private void showAlertIconDialog() {
        // Prepare grid view
        GridView gridView = new GridView(getActivity());

        gridView.setAdapter(iconAdapter);
        gridView.setNumColumns(3);
        gridView.setBackgroundColor(tmpItem.background);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(gridView);
        final AlertDialog dialog = builder.show();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tmpItem.icon = resIds[position];
                selectedIcon.setImageResource(tmpItem.icon);
                dialog.dismiss();
            }
        });
    }
}
