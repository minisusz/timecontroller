package pw.minisusz.timecontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import pw.minisusz.timecontroller.Services.AreaService;
import pw.minisusz.timecontroller.Services.IAreaService;

public abstract class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    FrameLayout frameLayout;
    NavigationView navigationView;
    TextView locationTextView;
    View locationView;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        IAreaService areaService = new AreaService(getApplicationContext());
        locationTextView.setText(areaService.getNameCurrentArea());

        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    locationTextView.setText((String) intent.getExtras().get("nameArea"));
                }
            };
            registerReceiver(broadcastReceiver, new IntentFilter("LOCATION_UPDATE"));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        locationView = findViewById(R.id.location_bar);
        boolean isDetectingEnabled = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("switch_detecting", false);
        if (isDetectingEnabled)
            locationView.setVisibility(View.VISIBLE);
        else
            locationView.setVisibility(View.GONE);
        locationTextView = (TextView) findViewById(R.id.my_location);
        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        overridePendingTransition(R.anim.fadeout, R.anim.fadein);

        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) != Configuration.SCREENLAYOUT_SIZE_LARGE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //to prevent current item select over and over
        if (item.isChecked()) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return false;
        }
        if (id == R.id.nav_home) {
            Intent intent = new Intent();
            intent.setClassName(this, "pw.minisusz.timecontroller.HomeActivity");
            startActivity(intent);
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent();
            intent.setClassName(this, "pw.minisusz.timecontroller.HistoryActivity");
            startActivity(intent);
        } else if (id == R.id.nav_activities) {
            Intent intent = new Intent();
            intent.setClassName(this, "pw.minisusz.timecontroller.ActivitiesActivity");
            startActivity(intent);
        } else if (id == R.id.nav_notifications) {
            Intent intent = new Intent();
            intent.setClassName(this, "pw.minisusz.timecontroller.NotificationsActivity");
            startActivity(intent);
        } else if (id == R.id.nav_locations) {
            Intent intent = new Intent();
            intent.setClassName(this, "pw.minisusz.timecontroller.LocationsActivity");
            startActivity(intent);
        }

        finish();
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
